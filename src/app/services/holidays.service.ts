import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
const url = 'https://apis.digital.gob.cl/';
const path = 'fl/feriados';
@Injectable({
  providedIn: 'root'
})
export class HolidaysService {

  constructor(private http: HttpClient) { 

  }
  getAll() {
    return this.http.get(url + path );
  }
  getYear(year) {
    return this.http.get(url + path + '/' + year);
  }
  getMonth(year,month) {
    return this.http.get(url + path + '/' + year + '/' + month);
  }

}
