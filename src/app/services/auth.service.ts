import { Injectable} from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { User } from '../interfaces/user.interface';

import { environment } from './../../environments/environment';


@Injectable( {
  providedIn: 'root'
})
export class AuthService {
  public user: User;
  constructor(private http: HttpClient) {
    if (localStorage.user) {
      this.user = localStorage.user;
    }
  }
  auth(rut: string, remember: boolean, withUser: boolean) {
    const self = this;
    return new Promise((resolve, reject) => {
      const  httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          Authorization: 'Basic ' + btoa(environment.user + ':' + environment.password)
        })
      };
      this.http.get( environment.apiUrl + environment.client + '/token',  httpOptions).pipe().subscribe((res: any) => {
        const token = res.result.token;
        localStorage.setItem('access_token', token);
        if (!withUser) {
          resolve(token);
        } else {
          const  options = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              Authorization: 'Basic ' + token
            })
          };
          self.http.get( environment.apiUrl + environment.client + '/person/rut/' + rut,  options).pipe().subscribe((response: any) => {
            const uuser: User = response.result;
            uuser.token = token;
            uuser.authorized = true;
            this.user = uuser;
            if (remember) {
              localStorage.setItem('user', JSON.stringify(uuser));
            } else {
              localStorage.removeItem('user');
            }
            Object.assign(self.user, uuser);
            resolve(uuser);
          });
        }
      });
    });
  }


}
