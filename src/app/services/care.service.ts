import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AuthService } from './auth.service';
import { environment } from './../../environments/environment';

import { User } from '../interfaces/user.interface';
import { Observable } from 'rxjs';

@Injectable( {
  providedIn: 'root'
})
export class CareService {
  public user: User = {
    authorized: false,
    duration: 0,
    document: '',
    token: '',
    document_type: '',
    first_name: '',
    fnacim: 0,
    isapre: '',
    last_name: '',
    nombre: '',
    sexo: 0,
    show_helps: {
      agenda: 0, doctor: 0, office: 0, speciality: 0
    },
    agenda: 0,
    doctor: 0,
    office: 0,
    speciality: 0,
  };
  auth: AuthService;
  constructor(private http: HttpClient, auth: AuthService) {
    this.auth = auth;

   }
  init() {
    return this.auth.auth( '', false, false);
  }
  enter(run, remmenber) {
    const $this = this;
    return this.auth.auth( run, remmenber, true).then( (r) => {
      Object.assign($this.user, r);
    });
  }
  config() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic ' + localStorage.getItem('access_token')
      })
    };
    return this.http.get( environment.apiUrl + environment.client + '/agenda', httpOptions);
  }
  searchProfessionals(name) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic ' + localStorage.getItem('access_token')
      })
    };
    return this.http.get( environment.apiUrl + environment.client + '/professionals/' + name, httpOptions);
  }
  searchSpecialitiesId(id) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic ' + localStorage.getItem('access_token')
      })
    };
    return this.http.get( environment.apiUrl  + environment.client + '/specialities/professionals_list/' + id, httpOptions);
  }
  searchSpecialities(name) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic ' + localStorage.getItem('access_token')
      })
    };
    return this.http.get( environment.apiUrl  + environment.client + '/specialities/' + name, httpOptions);
  }
  searchConditions(name) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic ' + localStorage.getItem('access_token')
      })
    };
    return this.http.get( environment.apiUrl + environment.client + '/conditions/' + name, httpOptions);
  }

  getUser(): any {
    const UserObservable = new Observable(observer => {
      observer.next(this.user);
    });
    return UserObservable;
  }
  getscheduleDay( idDoctor, idSpecialities, correlative, filter) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic ' + localStorage.getItem('access_token')
      })
    };
    return this.http.get( environment.apiUrl + environment.client + '/agenda/day/professional/' + idDoctor + '/' + idSpecialities + '/' + correlative + '?' + filter, httpOptions);
  }

  getscheduleMonth( idDoctor, idSpecialities, correlative, filter) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic ' + localStorage.getItem('access_token')
      })
    };
    return this.http.get( environment.apiUrl + environment.client + '/agenda/month/professional/' + idDoctor + '/' + idSpecialities + '/' + correlative + '?' + filter, httpOptions);
  }
  getscheduleUser( rut ) {
    const rutClear = rut.substring(2, rut.length);
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic ' + localStorage.getItem('access_token')
      })
    };
    return this.http.get( environment.apiUrl + environment.client + '/reservas/' + rutClear, httpOptions);
  }
  setSchedule(doctor: any, user: any) {

    const nameDoc = doctor._source.full_name.replace(/<\/?[^>]+(>|$)/g, '').split(' ', 3);

    const lastName = user.last_name.split(' ');

    const agenda = {
      rut_medico: doctor._source.rut.substring(2, doctor._source.rut.length),
      cod_medico: doctor._source.professional_code,
      nombre_medico: nameDoc[0],
      paterno_medico: nameDoc[1],
      materno_medico: nameDoc[2],
      centro: doctor.schedule.centro,
      cod_especialidad: doctor._source.specialty_code,
      especialidad: doctor._source.specialty_name,
      correlativo_agenda: doctor._source.correlativo_agenda,
      corr_horario: doctor.schedule.CorrelativoHorario,
      fecha_reserva: doctor.schedule.dayString,
      hora_reserva: doctor.schedule.Hora,
      box: doctor.schedule.Box,
      paciente: {
        documento: user.document_type,
        tipo_documento: user.rut.substring(2, doctor._source.rut.length),
        nombres: user.first_name,
        paterno: lastName[0],
        materno: lastName[1],
        direccion: user.direccion,
        ciudad: user.ciudad,
        id_paciente: user.id_paciente,
        cod_isapre: user.cod_isapre,
        emails: [user.email],
        phones: [user.phone]
      },
      user: 'agenda'
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: 'Basic ' + localStorage.getItem('access_token')
      })
    };
    return this.http.post( environment.apiUrl + environment.client + '/agenda', agenda, httpOptions);
  }

  logout() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('user');
    localStorage.removeItem('config');
  }
}
