import { Injectable } from '@angular/core';

@Injectable()
export class Globals {

    selectedSearch: any = [];
    selectedCentersStart: any = [
        {nombre_centro: 'Filtrar por lugar'}
    ];
    selectedCenters: any = this.selectedCentersStart;
    selectedHourAll: any = [
        {name: 'Filtrar por Hora / Día', value: true},
        {name: 'Horario A.M', value: false},
        {name: 'Horario P.M', value: false},
        {name: 'Hora de almuerzo 13:00 a 15:00 hrs.', value: false},
        {name: 'lun', value: false},
        {name: 'mar', value: false},
        {name: 'mie', value: false},
        {name: 'jue', value: false},
        {name: 'vie', value: false},
        {name: 'sab', value: false},
        {name: 'dom', value: false},
    ];
    selectedHour: any = [
        {name: 'Filtrar por Hora / Día', value: true},
    ];

}
