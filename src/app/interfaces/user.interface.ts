export interface User {
    authorized?: boolean;
    duration?: number;
    document?: string;
    token?: string;
    document_type?: string;
    first_name?: string;
    fnacim?: number;
    isapre?: string;
    last_name?: string;
    nombre?: string;
    sexo?: number;
    show_helps: {
        agenda?: number, doctor?: number, office?: number, speciality?: number
    };
    agenda?: number;
    doctor?: number;
    office?: number;
    speciality?: number;
}
