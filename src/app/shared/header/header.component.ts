import { Component, OnInit, Input } from '@angular/core';
import {User} from '../../interfaces/user.interface';
import {CareService} from '../../services/care.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public user: User = {
    authorized: false,
    duration: 0,
    document: '',
    token: '',
    document_type: '',
    first_name: '',
    fnacim: 0,
    isapre: '',
    last_name: '',
    nombre: '',
    sexo: 0,
    show_helps: {
      agenda: 0, doctor: 0, office: 0, speciality: 0
    },
    agenda: 0,
    doctor: 0,
    office: 0,
    speciality: 0,
  };
  active: boolean;
  config: any;
  constructor(public care: CareService) {
    this.config = JSON.parse(localStorage.getItem('config'));
    this.active = false;
  }

   ngOnInit() {
     this.user = JSON.parse(localStorage.getItem('user'));
  }
}
