import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import {BuscadorComponent} from './pages/buscador/buscador.component';
import {CalendarioComponent} from './pages/calendario/calendario.component';
import {DoctoresComponent} from './pages/doctores/doctores.component';
import {ReservaComponent} from './pages/reserva/reserva.component';
import {ValidarComponent} from './pages/validar/validar.component';
import {MisCitasComponent} from './pages/mis-citas/mis-citas.component';
import {HorasDiaComponent} from './pages/horas-dia/horas-dia.component';
import {ReservaConfirmacionComponent} from './pages/reserva-confirmacion/reserva-confirmacion.component';
import {AuthGuard} from './guards/auth.guard';




const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'buscador', component: BuscadorComponent, canActivate: [AuthGuard] },
  { path: 'calendario', component: CalendarioComponent, canActivate: [AuthGuard] },
  { path: 'doctores', component: DoctoresComponent, canActivate: [AuthGuard] },
  { path: 'horas-dia', component: HorasDiaComponent, canActivate: [AuthGuard] },
  { path: 'reserva', component: ReservaComponent , canActivate: [AuthGuard]},
  { path: 'validar', component: ValidarComponent , canActivate: [AuthGuard]},
  { path: 'mis-citas', component: MisCitasComponent , canActivate: [AuthGuard]},
  { path: 'reserva-confirmacion', component: ReservaConfirmacionComponent , canActivate: [AuthGuard]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
