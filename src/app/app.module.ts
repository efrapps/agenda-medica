import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


// SERVICES
import {HolidaysService} from './services/holidays.service';
import {CareService} from './services/care.service';
import {AuthService} from './services/auth.service';


// import globals
import { Globals } from './globals';

// import internal modules
import {SharedModule} from './shared/shared.module';
import {PagesModule} from './pages/pages.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    PagesModule
  ],
  providers: [HolidaysService, CareService, AuthService, Globals],
  bootstrap: [AppComponent]
})
export class AppModule { }
