import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { trigger, style, animate, transition, state } from '@angular/animations';

@Component({
  selector: 'app-toggle-filtro-centro-medico',
  templateUrl: './toggle-filtro-centro-medico.component.html',
  styleUrls: ['./toggle-filtro-centro-medico.component.scss'],
  animations: [
    trigger('EnterLeave', [

      transition(':enter', [
        style({ transform: 'translateY(100%)' }),
        animate('0.3s ease-out', style({ transform: 'translateY(0%)' }))
      ]),
      transition(':leave', [
        style({ transform: 'translateY(0%)' }),
        animate('0.3s ease-out', style({ transform: 'translateY(100%)' }))
      ])
    ])
  ],
})
export class ToggleFiltroCentroMedicoComponent implements OnInit {

  centers$: any = [];
  selectedCenters = [];
  centers: any = [];
  constructor() {
  }
  @Input() placeFilter: boolean;
  @Output() changePlace: EventEmitter<any> = new EventEmitter<any>();
  ngOnInit() {
    const config = JSON.parse(localStorage.getItem('config'));
    const centers = config.result.centros;
    centers.forEach(element => {
      this.centers$.push(element);
    });
  }
  clearModel() {
    this.selectedCenters = [];
  }
  doSomething(event) {
    if (event.length === 0) {
      this.centers = [
        {nombre_centro: 'Filtrar por lugar'}
      ];
    } else {
      this.centers = event;
    }
    localStorage.setItem('centers', JSON.stringify(this.centers));
    this.changePlace.emit(this.centers);
  }



}
