import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleFiltroCentroMedicoComponent } from './toggle-filtro-centro-medico.component';

describe('ToggleFiltroCentroMedicoComponent', () => {
  let component: ToggleFiltroCentroMedicoComponent;
  let fixture: ComponentFixture<ToggleFiltroCentroMedicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToggleFiltroCentroMedicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleFiltroCentroMedicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
