import { ElementRef, Component, Inject, ChangeDetectionStrategy ,  ViewEncapsulation
} from '@angular/core';
import {
  CalendarEvent,
  CalendarDateFormatter,
  DAYS_OF_WEEK
} from 'angular-calendar';
import { CustomDateFormatter } from './custom-date-formatter.provider';
import { DOCUMENT } from '@angular/common';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,

  isSameMonth,
  subMonths,
  subWeeks,
  addWeeks,
  addMonths
} from 'date-fns';
import {CareService} from '../../services/care.service';
import {formatDate} from '@angular/common';
import { Observable, Subject } from 'rxjs';
const colors: any = {
  copado: {
    primary: '#002FA6',
    secondary: '#FAE3E3'
  },
  disponible: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  nodisponible: {
    primary: 'grey',
    secondary: 'grey'
  },
  holidays : {
    primary: '#8b0000',
    secondary: '#FAE3E3'
  }

};

import { Router } from '@angular/router';
import {map} from 'rxjs/operators';
interface Agenda {
  title: string;
  start: string;
  end: string;
  color: string;
  type: string;
  cssClass: string;
}
@Component({
  selector: 'app-angular-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './angular-calendar.component.html',
  styleUrls: [

    './angular-calendar.component.scss'
  ],
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatter
    }
  ],
  encapsulation: ViewEncapsulation.None
})

export class AngularCalendarComponent {
  view = 'month';

  viewDate: Date = new Date();
  refresh: Subject<any> = new Subject();
  events: CalendarEvent[] = [];
  events$: Observable<CalendarEvent<{ event: any }>[]>;
  todayDate = new Date();
  locale = 'es';
  day = true;
  weekStartsOn: number = DAYS_OF_WEEK.MONDAY;

  weekendDays: number[] = [DAYS_OF_WEEK.SUNDAY];


  private ThemeClass = 'clinic-theme';

  constructor(@Inject(DOCUMENT) private document, private router: Router, private care: CareService, private elRef: ElementRef) {

    const format = 'dd/MM/yyyy';
    const locale = 'es-CL';
    const formattedDate = formatDate(this.todayDate, format, locale);
    this.getscheduleMonth(formattedDate);
  }
  async getscheduleMonth(day) {
    const self = this;
    const doctor = JSON.parse(localStorage.getItem('doctorSelected'));

    this.events$ = self.care.getscheduleMonth(
      doctor._source.professional_code,
      doctor._source.specialty_code,
      doctor._source.correlativo_agenda,
      'filters={"start_date": "' + day + '", "centros": ["3501","17014"]}'
    ).pipe(
      map(({ results }: { results: any[] }) => {
        return results.map((value, key ) => {
          const today = new Date();
          today.setDate(new Date().getDate() + key);
          switch (value) {
            case '0':
              return {
                title: 'No Atiende',
                start: startOfDay(today),
                end: endOfDay(today),
                color: colors.copado,
                type: value,
                cssClass: 'no-click'
              };
              break;
            case '1':
              return {
                title: 'Disponible',
                start: startOfDay(today),
                end: endOfDay(today),
                color: colors.disponible,
                type: value,
                cssClass: 'click'
              };
              break;
            case '2':
              return {
                title: 'Copado',
                start: startOfDay(today),
                end: endOfDay(today),
                color: colors.nodisponible,
                type: value,
                cssClass: 'no-click'
              };
              break;
          }
        });
      })
    );


  }
  ngOnInit(): void {
    this.document.body.classList.add(this.ThemeClass);

  }

  ngOnDestroy(): void {
    this.document.body.classList.remove(this.ThemeClass);

  }
  dayClicked(date, events): void {
    if ( events.day.events[0].title === 'Disponible' ) {
      this.router.navigate(['/horas-dia', {day: events.day.date.toString()}]);
    }
    console.log();

  }
  today(): void {
    this.viewDate = new Date();
  }
  increment(): void {
    const addFn: any = {
      day: addDays,
      week: addWeeks,
      month: addMonths
    }[this.view];

    this.viewDate = addFn(this.viewDate, 1);

  }
  decrement(): void {
    const subFn: any = {
      day: subDays,
      week: subWeeks,
      month: subMonths
    }[this.view];
    this.viewDate = subFn(this.viewDate, 1);
  }


}
