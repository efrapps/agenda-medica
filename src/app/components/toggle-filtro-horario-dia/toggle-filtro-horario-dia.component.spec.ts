import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToggleFiltroHorarioDiaComponent } from './toggle-filtro-horario-dia.component';

describe('ToggleFiltroHorarioDiaComponent', () => {
  let component: ToggleFiltroHorarioDiaComponent;
  let fixture: ComponentFixture<ToggleFiltroHorarioDiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToggleFiltroHorarioDiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleFiltroHorarioDiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
