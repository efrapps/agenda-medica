import { Component, OnInit, Input } from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import {Globals} from '../../globals';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-toggle-filtro-horario-dia',
  templateUrl: './toggle-filtro-horario-dia.component.html',
  styleUrls: ['./toggle-filtro-horario-dia.component.scss'],
  animations: [
    trigger('EnterLeave', [

      transition(':enter', [
        style({ transform: 'translateY(100%)' }),
        animate('0.3s ease-out', style({ transform: 'translateY(0%)' }))
      ]),
      transition(':leave', [
        style({ transform: 'translateY(0%)' }),
        animate('0.3s ease-out', style({ transform: 'translateY(100%)' }))
      ])
    ])
  ],
})
export class ToggleFiltroHorarioDiaComponent implements OnInit {
  form: FormGroup;
    constructor(public globals: Globals, fb: FormBuilder) {

    this.form = fb.group({
      checkArray: fb.array([])
    });
   }
  @Input() timeFilter: boolean;
  ngOnInit() {
  }
  onCheckboxChange(e) {
    const checkArray: FormArray = this.form.get('checkArray') as FormArray;

    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
      this.globals.selectedHour = [];
      checkArray.value.forEach(element => {
        this.globals.selectedHour.push({name: element});
      });
    } else {
      let i = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          checkArray.removeAt(i);
          this.globals.selectedHour = [];
          if (!checkArray.value.length) {
            this.globals.selectedHour.push({name: 'Filtrar por Hora / Día'});
            return;
          }
          checkArray.value.forEach(element => {
            this.globals.selectedHour.push({name: element});
          });
          return;
        }
        i++;
      });
    }
  }

}
