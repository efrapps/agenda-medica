import { Component, OnInit } from '@angular/core';
import {Globals} from '../../globals';
import {ActivatedRoute, Router} from '@angular/router';
import { CareService } from 'src/app/services/care.service';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-horas-dia',
  templateUrl: './horas-dia.component.html',
  styleUrls: ['./horas-dia.component.scss']
})
export class HorasDiaComponent implements OnInit {
  placeFilter = false;
  timeFilter = false;
  result: any;
  todayDate: Date = new Date();
  hourPerDay: any[] = [];
  config: any;
  loadingList = false;
  noresult = false;
  constructor(
    public globals: Globals,
    public router: Router,
    public care: CareService,
    public route: ActivatedRoute
  ) {
    this.result  = JSON.parse(localStorage.doctorSelected);
    this.config =  JSON.parse(localStorage.config);
    const calendarDay = this.route.snapshot.paramMap.get('day');
    if (calendarDay) {
      this.todayDate = new Date(calendarDay);
    }
    const format = 'dd/MM/yyyy';
    const locale = 'es-CL';
    const formattedDate = formatDate(this.todayDate, format, locale);
    if (calendarDay) {
      this.OnscheduleDay(formattedDate, calendarDay);
    } else {
      this.onSearchHour(formattedDate);
    }
  }

  ngOnInit() {
    this.result  = JSON.parse(localStorage.doctorSelected);
    this.config =  JSON.parse(localStorage.config);
  }
  onInteractionClick($event) {
    this.result.schedule = $event;
    localStorage.setItem('doctorSelected', JSON.stringify(this.result)); // SOLO DESARROLLO
    this.router.navigate(['/reserva']);
  }

  onSearchHour(day) {
    this.loadingList = true;
    const self = this;
    new Promise((resolve, reject) => {
      self.care.getscheduleMonth(
        self.result._source.professional_code,
        self.result._source.specialty_code,
        self.result._source.correlativo_agenda,
        'filters={"start_date": "' + day + '", "centros": ["3501","17014"]}'
      ).subscribe((res: any) => {
        resolve(res.results);
      });
    }).then((data: any) => {
      const promises = [];
      const limit = 3;
      console.log(data.length);
      if (data.length > 0) {
        data.some((value, key) => {
          if (value === '1') {
            if (promises.length === limit) {
              return;
            }
            promises.push(self.onOrderDays(key));
          }
        });
        if (promises.length > 0) {
          Promise.all(promises).then((listHours: any) => {
            listHours.map((listHour) => {
              self.hourPerDay.push(listHour);
              self.loadingList = false;
            });
          });
        } else {
          self.loadingList = false;
          self.noresult = true;
        }
      } else {
        self.loadingList = false;
        self.noresult = true;

      }

    });
  }
  OnscheduleDay(key, calendarDay) {
    const self = this;
    self.loadingList = true;
    return  new Promise( (resolve, reject) => {
      self.care.getscheduleDay(
        self.result._source.professional_code,
        self.result._source.specialty_code,
        self.result._source.correlativo_agenda,
        'filters={"start_date": "' + key + '", "centros": ["3501","17014"]}'
      ).subscribe( (res: any) => {
        const scheduleDay: any = [];
        res.results.some( (box: any) => {
          if (box.Lleno === '0') {
            box.dayString = key;
            const format = 'EEE-dd';
            const locale = 'es-CL';
            const today = new Date(calendarDay);
            today.setDate(today.getDate());
            const dayFormat = formatDate(today, format, locale);
            box.day = dayFormat;
            box.dayStringFull = formatDate(today, 'full', locale);
            box.dayStringLong = formatDate(today, 'longDate', locale);
            self.config.result.centros.map( (data) => {
              if (data.codigo_centro === box.centro) {
                box.nombre_centro = data.nombre_centro;
                box.direccion = data.direccion;
              }
            });
            scheduleDay.push(box);
          }
        });
        resolve(scheduleDay);
      }) ;
    }).then( (data: any) => {
      data.map((listHour, c) => {
        if (c === 0) {
          self.hourPerDay.push([listHour]);
        } else {
          self.hourPerDay[0].push(listHour);
        }
        self.loadingList = false;
      });
    });
  }
  onOrderDays(key) {
    const self = this;
    return  new Promise( (resolve, reject) => {
      const format = 'dd/MM/yyyy';
      const locale = 'es-CL';
      const today = new Date();
      today.setDate(new Date().getDate() + key);
      const day = formatDate(today, format, locale);
      self.care.getscheduleDay(
        self.result._source.professional_code,
        self.result._source.specialty_code,
        self.result._source.correlativo_agenda,
        'filters={"start_date": "' + day + '", "centros": ["3501","17014"]}'
      ).subscribe( (res: any) => {
        const scheduleDay: any[] = [];
        res.results.some( (box: any) => {
          if (box.Lleno === '0') {

            box.dayString = day;
            const format = 'EEE-dd';
            const locale = 'es-CL';
            const today = new Date();
            today.setDate(new Date().getDate() + key);
            const dayFormat = formatDate(today, format, locale);

            box.day = dayFormat;
            box.dayStringFull = formatDate(today, 'full', locale);
            box.dayStringLong = formatDate(today, 'longDate', locale);
            self.config.result.centros.map( (data) => {
              if (data.codigo_centro === box.centro) {
                box.nombre_centro = data.nombre_centro;
                box.direccion = data.direccion;
              }
            });

            scheduleDay.push(box);

            return true;
          }
        });
        resolve(scheduleDay);
      });
    });
  }
}
