import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorasDiaComponent } from './horas-dia.component';

describe('HorasDiaComponent', () => {
  let component: HorasDiaComponent;
  let fixture: ComponentFixture<HorasDiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorasDiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorasDiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
