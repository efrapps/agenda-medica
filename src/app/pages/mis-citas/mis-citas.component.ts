import { Component, OnInit } from '@angular/core';
import {CareService} from '../../services/care.service';

@Component({
  selector: 'app-mis-citas',
  templateUrl: './mis-citas.component.html',
  styleUrls: ['./mis-citas.component.scss']
})
export class MisCitasComponent implements OnInit {
  citas: any = [];
  user: any;
  loadingList: boolean;
  constructor(public care: CareService) {
    this.loadingList = true;
    this.user = JSON.parse(localStorage.getItem('user'));
    const self = this;
    new Promise((resolve, reject) => {
      self.care.getscheduleUser(self.user.rut).subscribe((response: any) => {
          response.results.map((cita) => {
            self.citas.push(cita);
          });
          self.loadingList  = false;
      });
    });
  }

  ngOnInit() {
  }

}
