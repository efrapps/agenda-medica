import { Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CareService } from 'src/app/services/care.service';
import { Router } from '@angular/router';
import { trigger, transition, style, animate } from '@angular/animations';
declare function init_query();

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.scss'],
  animations: [
    trigger('myInsertRemoveTrigger', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('300ms', style({ opacity: 1 })),
      ]),
      transition(':leave', [
        animate('300ms', style({ opacity: 0 }))
      ])
    ])
  ]

})
export class BuscadorComponent implements OnInit {
  placeFilter = false;
  timeFilter = false;
  reactiveForm: FormGroup;
  sintoma: any = [];
  doctor: any = [];
  especialidad: any = [];
  active = true;
  loadingList = false;
  profesionalCode: number[] = [];
  centers: any[] = [];
  hours: any[] = [];
  constructor(fb: FormBuilder, public care: CareService, public route: Router) {
    this.reactiveForm = fb.group({
      search: ['', [Validators.required, Validators.minLength]]
    });
    this.centers.push({nombre_centro: 'Filtrar por lugar'});
    this.hours.push({name: 'Filtrar por Hora / Día', value: true});
  }
  ngOnInit() {
    init_query();
  }


  onSaveDoc(doc) {
    const limit = 3;
    if ( this.profesionalCode.indexOf(doc._source.professional_code) === -1 ) {
      if (this.doctor.length < limit) {
        this.loadingList = false;
        this.active = false;
        this.doctor.push(doc);
        this.profesionalCode.push(doc._source.professional_code);
      }
    }

  }
  async onSearch() {
    const valid = this.reactiveForm.valid;
    const value = this.reactiveForm.value.search;
    const limit = 3;
    const self = this;
    if (valid) {
      if (this.doctor === 0) {
        this.loadingList = true;
      }

      new Promise((resolve, reject) => {
        self.care.searchProfessionals(value).subscribe((res: any) => {
          resolve(res.results);
        });
      }).then((data: any) => {
        data.some((d) => {
          d._source.full_name = d._source.full_name.toLowerCase().replace(value, '<strong class="search-letter">' + value + '</strong>');
          self.onSaveDoc(d);
        });
      });
      new Promise((resolve, reject) => {
        self.care.searchSpecialities(value).subscribe((res: any) => {
          resolve(res.results);
        });
      }).then((data: any) => {
        data.some((d) => {
          d._source.specialty_name = d._source.specialty_name.toLowerCase().replace(value, '<strong class="search-letter">' + value + '</strong>');
          self.especialidad.push(d);
          if (self.especialidad.length === limit) {
            self.loadingList = false;
            self.active = false;
            return;
          }
        });
      });

    } else {
      self.doctor = [];
      self.sintoma = [];
      self.especialidad = [];
      self.loadingList = false;
      self.active = true;
    }
  }



  onInteractionClick($event) {
    localStorage.setItem('doctorSelected', JSON.stringify($event)); // SOLO DESARROLLO
    this.route.navigate(['/horas-dia']);
  }
  onInteractionClickList($event) {
    localStorage.setItem('specialitiesSelected', JSON.stringify($event)); // SOLO DESARROLLO
    this.route.navigate(['/doctores']);
  }

}
