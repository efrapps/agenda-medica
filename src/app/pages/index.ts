// import pages
export { LoginComponent } from './login/login.component';
export { BuscadorComponent } from './buscador/buscador.component';
export { CalendarioComponent } from './calendario/calendario.component';
export { DoctoresComponent } from './doctores/doctores.component';
export { ReservaComponent } from './reserva/reserva.component';
export { MisCitasComponent } from './mis-citas/mis-citas.component';
export { ReservaConfirmacionComponent } from './reserva-confirmacion/reserva-confirmacion.component';
export { HorasDiaComponent } from './horas-dia/horas-dia.component';
export { ValidarComponent } from './validar/validar.component';

