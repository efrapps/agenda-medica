import { Component, OnInit } from '@angular/core';
import {Globals} from '../../globals';
import { Router } from '@angular/router';

@Component({
  selector: 'app-calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.scss']
})
export class CalendarioComponent implements OnInit {
  placeFilter = false;
  timeFilter = false;
  data: any;
  constructor( private route: Router) {
    this.data = JSON.parse(localStorage.getItem('doctorSelected'));
  }

  ngOnInit() {
  }

}
