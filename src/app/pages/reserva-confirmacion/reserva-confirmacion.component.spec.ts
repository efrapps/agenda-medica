import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReservaConfirmacionComponent } from './reserva-confirmacion.component';

describe('ReservaConfirmacionComponent', () => {
  let component: ReservaConfirmacionComponent;
  let fixture: ComponentFixture<ReservaConfirmacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReservaConfirmacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReservaConfirmacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
