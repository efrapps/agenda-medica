import { Component, OnInit } from '@angular/core';
import * as Utils from '../../utils';
@Component({
  selector: 'app-reserva-confirmacion',
  templateUrl: './reserva-confirmacion.component.html',
  styleUrls: ['./reserva-confirmacion.component.scss']
})
export class ReservaConfirmacionComponent implements OnInit {

  constructor() { }
  result: any;
  agenda: any;
  user: any;
  ngOnInit() {
    this.result = JSON.parse(localStorage.getItem('doctorSelected'));
    this.agenda = JSON.parse(localStorage.getItem('agenda'));
    this.user = JSON.parse(localStorage.getItem('user'));
    this.agenda.result.MONTO_PAGAR = Utils.transformers.currencyFormat(this.agenda.result.MONTO_PAGAR);
  }

}
