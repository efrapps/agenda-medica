import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-validar',
  templateUrl: './validar.component.html',
  styleUrls: ['./validar.component.scss']
})
export class ValidarComponent implements OnInit {
  result: any;
  config: any;
  constructor() {
    this.result = JSON.parse(localStorage.getItem('doctorSelected'));
  }

  ngOnInit() {
  }

}
