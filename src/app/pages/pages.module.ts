import {NgModule} from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
registerLocaleData(localeEs);
import {HttpClientModule} from '@angular/common/http';
import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import {Ng2Rut} from 'ng2-rut';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {ToggleFiltroCentroMedicoComponent} from '../components/toggle-filtro-centro-medico/toggle-filtro-centro-medico.component';
import {ToggleFiltroHorarioDiaComponent} from '../components/toggle-filtro-horario-dia/toggle-filtro-horario-dia.component';
import {BackButtonDirective} from '../directives/back-button.directive';
import {AngularCalendarComponent} from '../components/angular-calendar/angular-calendar.component';
import {LoginGoogleComponent} from '../components/login-google/login-google.component';

import {
  LoginComponent,
  BuscadorComponent,

  CalendarioComponent,
  DoctoresComponent,
  ReservaComponent,
  MisCitasComponent,
  ReservaConfirmacionComponent,
  HorasDiaComponent,
  ValidarComponent,
} from './index';

@NgModule({
  declarations: [
    LoginComponent,
    HorasDiaComponent,
    AngularCalendarComponent,
    LoginGoogleComponent,
    MisCitasComponent,
    ReservaConfirmacionComponent,
    BuscadorComponent,

    CalendarioComponent,
    DoctoresComponent,
    ReservaComponent,
    ValidarComponent,
    ToggleFiltroCentroMedicoComponent,
    ToggleFiltroHorarioDiaComponent,
    BackButtonDirective,
  ],
  exports: [
    LoginComponent,
    HorasDiaComponent,
    AngularCalendarComponent,
    LoginGoogleComponent,
    MisCitasComponent,
    ReservaConfirmacionComponent,
    BuscadorComponent,
    CalendarioComponent,
    DoctoresComponent,
    ReservaComponent,
    ValidarComponent,
    ToggleFiltroCentroMedicoComponent,
    ToggleFiltroHorarioDiaComponent,
    BackButtonDirective,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }),
    Ng2Rut,
    ReactiveFormsModule,
    FormsModule,
    NgSelectModule,
  ]
})
export class PagesModule {}
