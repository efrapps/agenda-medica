import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CareService} from '../../services/care.service';
import {Router} from '@angular/router';
import {Globals} from '../../globals';

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.scss']
})
export class ReservaComponent implements OnInit {
  reactiveForm: FormGroup;
  result: any;
  loading: boolean;
  constructor(fb: FormBuilder, public care: CareService, public route: Router, public globals: Globals) {
    this.loading = false;
    this.result = JSON.parse(localStorage.getItem('doctorSelected'));
    this.result.user = JSON.parse(localStorage.getItem('user'));
    this.reactiveForm = fb.group({
      phone: ['', [Validators.required, Validators.minLength]],
      email: ['', [Validators.required, Validators.minLength]],
      date: ['', [Validators.required]],
      prevision: ['', [Validators.required]],

    });
  }

  ngOnInit() {
  }
  onSubmit() {
    const valid = this.reactiveForm.valid;
    const self = this;
    self.loading = true;
    self.result.user.email = this.reactiveForm.value.email;
    self.result.user.phone =  this.reactiveForm.value.phone;
    localStorage.setItem('user', JSON.stringify(self.result.user) );
    if (valid) {
      return new Promise((resolve, reject) => {
        self.care.setSchedule(
          self.result,
          self.result.user
        ).subscribe((res: any) => {
          resolve(res);
        });
      }).then((data: any) => {
        if (data.result.CODIGO_PRESTA) {
          localStorage.setItem('agenda', JSON.stringify(data));
        }
        self.loading = false;
        self.route.navigate(['/validar']);
      }).catch((error) => {
        console.log(error);
      });
    }
  }
}
