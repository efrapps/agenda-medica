import { Component, OnInit , SimpleChanges, Input} from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {RutValidator} from 'ng2-rut';
import { Router } from '@angular/router';
import { User } from '../../interfaces/user.interface';
import {CareService} from '../../services/care.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  PrevIn: boolean = false;
  birthIn: boolean = false;
  submitted = false;
  reactiveForm: FormGroup;
  care: CareService;
  public user: User = {
    authorized: false,
    duration: 0,
    document: '',
    token: '',
    document_type: '',
    first_name: '',
    fnacim: 0,
    isapre: '',
    last_name: '',
    nombre: '',
    sexo: 0,
    show_helps: {
      agenda: 0, doctor: 0, office: 0, speciality: 0
    },
    agenda: 0,
    doctor: 0,
    office: 0,
    speciality: 0,
  };
  loading = false;
  config: any;
  body = document.body.classList;
  rut: string;
  constructor(fb: FormBuilder, rutValidator: RutValidator, private router: Router, care: CareService) {
    if (localStorage.getItem('user')) {
      const rememberUser = JSON.parse(localStorage.getItem('user'));
      this.rut = rememberUser.rut.slice(2, rememberUser.rut.length);
    }
    this.care = care;
    if (this.rut) {
      this.reactiveForm = fb.group({
        rut: [this.rut, [Validators.required, rutValidator]],
        rememberme: [false, []]
      });
    } else {
      this.reactiveForm = fb.group({
        rut: [ [Validators.required, rutValidator]],
        rememberme: [false, []]
      });
    }

  }
  ngOnInit() {
    this.config = JSON.parse(localStorage.getItem('config'));
    if (!this.config) {
      this.body.add('loading_login');
      const self = this;
      const promise = ( this.care.init() );
      promise.then( (res) => {
        self.care.config() .subscribe((config) => {
          self.config = config;
          self.body.remove('loading_login');
          localStorage.setItem('config', JSON.stringify(config));
      });
      });
    }
  }
  onSubmit() {
    this.submitted = true;
    if (this.reactiveForm.invalid) {
        return;
    }
    const self = this;
    const run = this.reactiveForm.value.rut.replace(/[-,\.]/gi, '');
    const rememberme = this.reactiveForm.value.rememberme;
    const last = run.slice(-1);
    const rest = run.slice(0, (run.length - 1));
    this.loading = true;
    const promise = ( this.care.enter( '00' + rest + '-' + last, true) );
    promise.then( (res) => {
      self.loading = false;
      self.router.navigateByUrl('/buscador');
    });
}
}
