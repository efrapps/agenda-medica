import { Component, OnInit } from '@angular/core';
import {Globals} from '../../globals';
import { Router } from '@angular/router';
import { CareService } from 'src/app/services/care.service';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-doctores',
  templateUrl: './doctores.component.html',
  styleUrls: ['./doctores.component.scss']
})
export class DoctoresComponent implements OnInit {
  placeFilter = false;
  timeFilter = false;
  result: any;

  schedule: any = [];
  docList: any = [];
  center: any;
  loadingList = true;

  profesionalCode: number[] = [];

  todayDate: Date = new Date();
  constructor(public globals: Globals, public route: Router, public care: CareService) {
    this.center = globals.selectedCenters;
    this.result = JSON.parse(localStorage.getItem('specialitiesSelected'));
    this.onProfesionalList(this.result._source.specialty_code);

  }
  onInteractionClick($event) {
    this.globals.selectedSearch = $event;
    localStorage.setItem('doctorSelected', JSON.stringify($event)); // SOLO DESARROLLO
    this.route.navigate(['/horas-dia']);
  }

  onProfesionalList(esp) {
    const self = this;
    new Promise(function(resolve, reject) {
      self.care.searchSpecialitiesId(esp).subscribe(function(res: any) {
        resolve(res.results.slice(1, res.results.length - 1));
      });
    }).then(function(data: any) {
      data.map(function(doc) {
        self.onSearchMonth(doc);
      });

    });
  }

  onSearchMonth(doc) {
    const self = this;
    const format = 'dd/MM/yyyy';
    const locale = 'es-CL';
    const formattedDate = formatDate(this.todayDate, format, locale);

    return new Promise(function(resolve, reject) {
      self.care.getscheduleMonth(
        doc._source.professional_code,
        doc._source.specialty_code,
        doc._source.correlativo_agenda,
        'filters={"start_date": "' + formattedDate + '", "centros": ["3501","17014"]}'
      ).subscribe(function(res: any) {
        const data: any = {
          result: res.results,
          doc
        };
        resolve(data);
      });
    }).then(function(data: any) {
      for (let x = 0; x < data.result.length; x++) {
        if (data.result[x] == '1') {
          self.onSearchSchedule(data.result[x], x, data.doc);
          break;
        }
      }
    });
  }
  onSearchSchedule(value, key, doc) {
    const self = this;
    const format = 'dd/MM/yyyy';
    const locale = 'es-CL';
    const today = new Date();
    today.setDate(new Date().getDate() + key);
    const day = formatDate(today, format, locale);
    const dayFull = formatDate(today, 'mediumDate', locale);
    return new Promise(function(resolve, reject) {
      self.care.getscheduleDay(
        doc._source.professional_code,
        doc._source.specialty_code,
        doc._source.correlativo_agenda,
        'filters={"start_date": "' + day + '", "centros": ["3501","17014"]}'
      ).subscribe(function(res: any) {
        res.results.map(function(box: any) {
          if (box.Lleno === '0') {
            doc.schedule = box;
            doc.day = dayFull;
            resolve(doc);
          }

        });

      });
    }).then(function(data: any) {
      self.onSaveDoc(data);
    });

  }
  onSaveDoc(doc) {
    const limit =  3;
    if (this.profesionalCode.indexOf(doc._source.professional_code) === -1) {
      const docNew  = doc;
      this.loadingList = false;
      this.docList.push(docNew);
      this.profesionalCode.push(docNew._source.professional_code);
    }

  }
  ngOnInit() {
  }

}
