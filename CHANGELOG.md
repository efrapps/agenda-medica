### 0.1.0 (2020-02-28)

##### Chores

* **package:**  add ng-rut (bf97d1de)
* **Calendar:**  add calendar-angular (41fc5993)
* **Package:**  update @angular/animations (5f02e4f6)

##### Documentation Changes

* **Changelog:**  update changelog (32d2fd1b)
* **Chanelog:**
  *  update changelog (3d2ebb11)
  *  add ChangeLog (e78022e6)
* **README:**  Delete readme copy (3af9df2c)

##### New Features

* **Api:**
  *  Model user (cfa5db1c)
  *  add service care (a65fc0aa)
  *  add auth service (dc400645)
* **Calendario:**
  *  add service holidays (465342c5)
  *  add event calendar (4f3af7c6)
  *   add calendar (angular-calendar) (e0b1aa2c)
* **Calendar:**  add-module calendar (66147a4e)
* **Link:**
  *  links to the normal flow are added (1eb7a384)
  *  add links and animatios filters (f0fb97c2)
* **HTML:**   html integration to angular (7a6f3cc3)

##### Bug Fixes

* **css/HTML:**
  *  update mobile style + add class for paddingTop (f55c77dc)
  *  change calendar style (18dd2aea)
* **css:**
  *  change calendar style v2 (379e8923)
  *  remove relative postition toggle desktop (7a4c30c4)
* **Calendario:**  change decrement and increment (2f10c268)
* **favicon:**  change favicon (9759d70a)

##### Other Changes

* **ng-bootstrap:**  add ng-bootstrap (346d3333)
* **README:**  add readme angular (f163fba1)

##### Tests

* **Api:**  test implementation api (d66b584e)

### 0.1.0 (2020-02-12)

##### Chores

* **package:**  add ng-rut (bf97d1de)
* **Calendar:**  add calendar-angular (41fc5993)
* **Package:**  update @angular/animations (5f02e4f6)

##### Documentation Changes

* **Chanelog:**
  *  update changelog (3d2ebb11)
  *  add ChangeLog (e78022e6)
* **README:**  Delete readme copy (3af9df2c)

##### New Features

* **Api:**
  *  Model user (cfa5db1c)
  *  add service care (a65fc0aa)
  *  add auth service (dc400645)
* **Calendario:**
  *  add service holidays (465342c5)
  *  add event calendar (4f3af7c6)
  *   add calendar (angular-calendar) (e0b1aa2c)
* **Calendar:**  add-module calendar (66147a4e)
* **Link:**
  *  links to the normal flow are added (1eb7a384)
  *  add links and animatios filters (f0fb97c2)
* **HTML:**   html integration to angular (7a6f3cc3)

##### Bug Fixes

* **css/HTML:**
  *  update mobile style + add class for paddingTop (f55c77dc)
  *  change calendar style (18dd2aea)
* **css:**
  *  change calendar style v2 (379e8923)
  *  remove relative postition toggle desktop (7a4c30c4)
* **Calendario:**  change decrement and increment (2f10c268)
* **favicon:**  change favicon (9759d70a)

##### Other Changes

* **ng-bootstrap:**  add ng-bootstrap (346d3333)
* **README:**  add readme angular (f163fba1)

##### Tests

* **Api:**  test implementation api (d66b584e)

#### 0.0.0 (2020-02-05)

##### Chores

* **Calendar:**  add calendar-angular (41fc5993)
* **Package:**  update @angular/animations (5f02e4f6)

##### Documentation Changes

* **Chanelog:**
  *  update changelog (3d2ebb11)
  *  add ChangeLog (e78022e6)
* **README:**  Delete readme copy (3af9df2c)

##### New Features

* **Calendario:**   add calendar (angular-calendar) (e0b1aa2c)
* **Calendar:**  add-module calendar (66147a4e)
* **Link:**
  *  links to the normal flow are added (1eb7a384)
  *  add links and animatios filters (f0fb97c2)
* **HTML:**   html integration to angular (7a6f3cc3)

##### Bug Fixes

* **favicon:**  change favicon (9759d70a)
* **css:**  remove relative postition toggle desktop (7a4c30c4)

##### Other Changes

* **ng-bootstrap:**  add ng-bootstrap (346d3333)
* **README:**  add readme angular (f163fba1)

#### 0.0.0 (2020-02-05)

##### Chores

* **Package:**  update @angular/animations (5f02e4f6)

##### Documentation Changes

* **README:**  Delete readme copy (3af9df2c)

##### New Features

* **Link:**
  *  links to the normal flow are added (014c4bf9)
  *  add links and animatios filters (f0fb97c2)
* **HTML:**   html integration to angular (7a6f3cc3)

##### Bug Fixes

* **favicon:**  change favicon (5ae7613b)
* **css:**  remove relative postition toggle desktop (7a4c30c4)

##### Other Changes

* **README:**  add readme angular (f163fba1)

